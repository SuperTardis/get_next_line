/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pporechn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/25 19:32:23 by pporechn          #+#    #+#             */
/*   Updated: 2016/11/28 15:10:34 by pporechn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *s, const char *s1)
{
	const char	*str1;
	const char	*str2;

	if (!*s1)
		return ((char*)s);
	while (*s)
	{
		str1 = s;
		str2 = s1;
		while (*str1 && *str2 && !(*str1 - *str2))
		{
			str1++;
			str2++;
		}
		if (!*str2)
			return ((char*)s);
		s++;
	}
	return (NULL);
}
