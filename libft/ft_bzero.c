/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pporechn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 14:32:24 by pporechn          #+#    #+#             */
/*   Updated: 2016/11/28 12:56:36 by pporechn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_bzero(void *s, size_t nul)
{
	size_t i;

	i = 0;
	while (nul > i)
	{
		((char*)s)[i] = '\0';
		i++;
	}
}
