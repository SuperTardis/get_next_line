/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pporechn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 15:33:26 by pporechn          #+#    #+#             */
/*   Updated: 2016/12/02 18:13:34 by pporechn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncpy(char *dest, const char *src, size_t n)
{
	char	*i;

	i = dest;
	while (n > 0 && *src != '\0')
	{
		*i = *src;
		i++;
		src++;
		n--;
	}
	while (n > 0)
	{
		*i = '\0';
		i++;
		n--;
	}
	return (dest);
}
