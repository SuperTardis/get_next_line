/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pporechn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 13:54:21 by pporechn          #+#    #+#             */
/*   Updated: 2016/11/28 15:52:14 by pporechn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	size_t	i;
	t_list	*ls;
	void	*tmp;

	tmp = NULL;
	i = 0;
	if (!(ls = (t_list *)malloc(sizeof(t_list))))
		return (NULL);
	if (content == NULL)
	{
		ls->content = NULL;
		ls->content_size = 0;
	}
	else
	{
		ls->content_size = content_size;
		ls->content = malloc(content_size);
		ft_memmove(ls->content, content, content_size);
	}
	ls->next = NULL;
	return (ls);
}
