/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pporechn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 14:55:47 by pporechn          #+#    #+#             */
/*   Updated: 2016/11/28 15:55:52 by pporechn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list	*tmp1;
	t_list	*tmp2;

	tmp1 = *alst;
	tmp2 = NULL;
	while (tmp1 != NULL)
	{
		del(tmp1->content, tmp1->content_size);
		tmp2 = tmp1->next;
		free(tmp1);
		tmp1 = tmp2;
	}
	*alst = NULL;
}
