/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pporechn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/24 14:57:47 by pporechn          #+#    #+#             */
/*   Updated: 2016/11/30 12:55:13 by pporechn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	*ft_str(char const *s, size_t i, size_t k)
{
	size_t	n;
	char	*str;

	n = 0;
	if (!(str = (char*)malloc((k - i) + 2)))
		return (NULL);
	while (i <= k)
	{
		str[n] = s[i];
		i++;
		n++;
	}
	str[n] = '\0';
	return (str);
}

char		*ft_strtrim(char const *s)
{
	size_t	i;
	size_t	k;
	size_t	len;

	i = 0;
	if (!s)
		return (NULL);
	len = ft_strlen(s);
	k = len;
	while (s[i] <= 32 && s[i] != '\0')
		i++;
	while (s[k] <= 32 && k > 0)
		k--;
	if (k > 0)
		return (ft_str(s, i, k));
	return (ft_strnew(1));
}
