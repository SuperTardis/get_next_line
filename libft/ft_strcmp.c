/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pporechn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 16:49:23 by pporechn          #+#    #+#             */
/*   Updated: 2016/11/28 13:12:59 by pporechn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strcmp(const char *s1, const char *s2)
{
	unsigned char	*tp1;
	unsigned char	*tp2;

	tp1 = (unsigned char*)s1;
	tp2 = (unsigned char*)s2;
	while (*tp1 == *tp2 && *tp1 != '\0' && *tp2 != '\0')
	{
		tp1++;
		tp2++;
	}
	if (*tp1 != *tp2)
		return (*tp1 - *tp2);
	return (0);
}
